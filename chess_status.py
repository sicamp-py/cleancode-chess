from enum import Enum


class ChessStatus(Enum):
    Ok = 0,
    Check = 1,
    Stalemate = 2,
    Mate = 3
