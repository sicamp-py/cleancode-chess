from chess_status import ChessStatus
from chess_tests.board_parser import BoardParser
from piece_color import PieceColor
from piece_type import King


class ChessProblem:

    Board = None,
    ChessStatus = ChessStatus.Ok

    @staticmethod
    def LoadFrom(lines):
        ChessProblem.Board = BoardParser().parse_board(lines)

    @staticmethod
    def CalculateChessStatus():
        isCheck = ChessProblem.IsCheckForWhite()
        hasMoves = False
        for locFrom in ChessProblem.Board.get_pieces(PieceColor.White):
            for locTo in ChessProblem.Board.get_piece(locFrom).get_moves(locFrom, ChessProblem.Board):
                old = ChessProblem.Board.get_piece(locTo)
                ChessProblem.Board.set(locTo, ChessProblem.Board.get_piece(locFrom))
                ChessProblem.Board.set(locFrom, None)
                if not ChessProblem.IsCheckForWhite():
                    hasMoves = True
                ChessProblem.Board.set(locFrom, ChessProblem.Board.get_piece(locTo))
                ChessProblem.Board.set(locTo, old)
        if isCheck:
            if hasMoves:
                ChessProblem.ChessStatus = ChessStatus.Check
            else: ChessProblem.ChessStatus = ChessStatus.Mate
        elif hasMoves: ChessProblem.ChessStatus = ChessStatus.Ok
        else: ChessProblem.ChessStatus = ChessStatus.Stalemate

    @staticmethod
    def IsCheckForWhite():
        isCheck = False
        for loc in ChessProblem.Board.get_pieces(PieceColor.Black):
            piece = ChessProblem.Board.get_piece(loc)
            moves = piece.get_moves(loc, ChessProblem.Board)
            for destination in moves:
                destinationPiece = ChessProblem.Board.get_piece(destination)
                if destinationPiece is not None and destinationPiece.is_(PieceColor.White, King):
                    isCheck = True
        if isCheck: return True
        return False
