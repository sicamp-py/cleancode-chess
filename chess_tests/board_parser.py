from board import Board
from piece import Piece
from piece_color import PieceColor
from piece_type import Rook, King, Queen, Bishop, Knight


class BoardParser:

    def parse_board(self, lines):
        if len(lines) != 8:
            raise Exception("Should be exactly 8 lines")
        for line in lines:
            if len(line) != 8:
                raise Exception("All lines should contain 8 chars")
        cells = [[None for y in range(8)] for x in range(8)]
        for i, line in enumerate(lines):
            for j, char in enumerate(line):
                cells[i][j] = self._parse_cell(char)
        return Board(cells)

    def _parse_cell(self, char):
        color = PieceColor.White if char.isupper() else PieceColor.Black
        type = self._parse_piece_type(char)
        return None if type is None else Piece(type, color)

    _types_mapping = {
        'R': Rook,
        'B': Bishop,
        'K': King,
        'Q': Queen,
        'N': Knight,
    }

    def _parse_piece_type(self, char):
        char = char.upper()
        if char in [' ', '.']:
            return None
        if char not in self._types_mapping:
            raise Exception('Unknown character on map: {0}'.format(char))
        return self._types_mapping[char]