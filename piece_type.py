from location import Location


class PieceType:

    def __init__(self, infinite, sign, moves):
        self._infinite = infinite
        self._sign = sign
        self._directions = self._get_directions(moves)

    @staticmethod
    def _get_directions(moves):
        set1 = set([Location(x, y) for x, y in moves])
        set2 = set([Location(-x, y) for x, y in moves])
        set3 = set([Location(x, -y) for x, y in moves])
        set4 = set([Location(-x, -y) for x, y in moves])
        return list(set1 | set2 | set3 | set4)

    def __str__(self):
        return self._sign

    def get_moves(self, location, board):
        all_moves = []
        for direction in self._directions:
            all_moves.extend(_moves_in_one_direction(location, board, direction, self._infinite))
        return all_moves

def _moves_in_one_direction(location, board, direction, infinite):
    piece = board.get_piece(location)
    distance = 100 if infinite else 1
    for i in range(1, distance + 1):
        to = Location(location.x + direction.x*i, location.y + direction.y*i)
        if not board.contains(to):
            break
        destination_piece = board.get_piece(to)
        if destination_piece is None:
            yield to
        else:
            if destination_piece.color != piece.color:
                yield to
            break


Rook = PieceType(infinite=True, sign='R', moves=[(0, 1), (1, 0)])
Queen = PieceType(infinite=True, sign='Q', moves=[(0, 1), (1, 0), (1, 1)])
Bishop = PieceType(infinite=True, sign='B', moves=[(1, 1)])
Knight = PieceType(infinite=False, sign='N', moves=[(2, 1), (1, 2)])
King = PieceType(infinite=False, sign='K', moves=[(0, 1), (1, 0), (1, 1)])
