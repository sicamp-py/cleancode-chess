from piece_color import PieceColor


class Piece:

    def __init__(self, type, color):
        self.type = type
        self.color = color

    def get_moves(self, location, board):
        return self.type.get_moves(location, board)

    def __str__(self):
        color = ' .' if self.type is None else ' ' + self.type
        return color.lower() if self.color == PieceColor.Black else color

    def is_(self, piece_color, piece_type=None):
        return self.color == piece_color and (piece_type is None or self.type == piece_type)
