from enum import Enum


class PieceColor(Enum):
    White = 0,
    Black = 1
