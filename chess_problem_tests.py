import glob
import pytest
from os import path

from chess_problem import ChessProblem
from chess_status import ChessStatus

chess_status_by_str = {
    'ok': ChessStatus.Ok,
    'mate': ChessStatus.Mate,
    'stalemate': ChessStatus.Stalemate,
    'check': ChessStatus.Check,
}


def test_repeated_method_call_do_not_change_behaviour():
    board = [
        "        ",
        "        ",
        "        ",
        "   q    ",
        "    K   ",
        " Q      ",
        "        ",
        "        ",
    ]
    ChessProblem.LoadFrom(board)
    ChessProblem.CalculateChessStatus()
    assert ChessStatus.Check == ChessProblem.ChessStatus

    # Now check that internal board modifications during the first call do not change answer
    ChessProblem.CalculateChessStatus()
    assert ChessStatus.Check == ChessProblem.ChessStatus


test_files = sorted(glob.glob(path.join(path.dirname(__file__), "chess_tests", "*.in")))
test_names = [path.basename(x) for x in test_files]


@pytest.mark.parametrize("test_file_path", test_files, ids=test_names)
def test_by_all_files(test_file_path):
    do_test_by_file(test_file_path)


def do_test_by_file(filename):
    with open(filename, 'r') as input_file:
        board = [line.strip() for line in input_file.readlines()]

    ChessProblem.LoadFrom(board)
    ChessProblem.CalculateChessStatus()

    with open(filename.replace('.in', '.ans'), 'r') as answer_file:
        answer = chess_status_by_str[answer_file.read().strip().lower()]

    assert answer == ChessProblem.ChessStatus
