from location import Location


class Board:

    def __init__(self, cells):
        self._cells = cells
        self._n = len(cells)
        self._m = len(cells[0])
        if not all([len(row) == self._m for row in cells]):
            raise Exception('Cells is not double-dimensional array')

    def get_piece(self, location):
        if not self.contains(location):
            return None
        return self._cells[location.x][location.y]

    def get_pieces(self, piece_color):
        def piece_at_location_has_color(location):
            piece = self.get_piece(location)
            if piece is None:
                return False
            return piece.is_(piece_color)

        return list(filter(piece_at_location_has_color, self._all_board()))

    def _all_board(self):
        return [Location(x, y) for x in range(self._n) for y in range(self._m)]

    def contains(self, location):
        return 0 <= location.x < self._n and 0 <= location.y < self._m

    def set(self, location, piece):
        self._cells[location.x][location.y] = piece


class TemporaryPieceMove:

    def __init__(self, board, location_from, location_to):
        self.board = board
        self.location_from = location_from
        self.location_to = location_to
        self.old_piece = None

    def __enter__(self):
        self.old_piece = self.board.get_piece(self.location_to)
        self.board.set(self.location_to, self.board.get_piece(self.location_from))
        self.board.set(self.location_from, None)

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.board.set(self.location_from, self.board.get_piece(self.location_to))
        self.board.set(self.location_to, self.old_piece)
